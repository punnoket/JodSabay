package com.compscitutorials.basigarcia.navigationdrawervideotutorial;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * Created by Administrator on 28/7/2559.
 */
public class CustomSwipeAdapter extends PagerAdapter {
    private int[] image =  {R.id.im1, R.id.im2, R.id.im3, R.id.im4};
    private Context context;
    private LayoutInflater layoutInflater;

    public CustomSwipeAdapter(Context context) {
        this.context = context;
    }
    @Override
    public int getCount() {
        return image.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (LinearLayout)object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.swip_layout, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.im1);
        imageView.setImageResource(image[position]);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}
