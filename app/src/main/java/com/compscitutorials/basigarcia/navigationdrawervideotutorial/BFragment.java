package com.compscitutorials.basigarcia.navigationdrawervideotutorial;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class BFragment extends Fragment {

    private ViewPager pager;
    CustomSwipeAdapter adapter;
    public BFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       /* pager = (ViewPager)
        adapter = new CustomSwipeAdapter(this.getContext());*/


        return inflater.inflate(R.layout.fragment_b, container, false);
    }


}
